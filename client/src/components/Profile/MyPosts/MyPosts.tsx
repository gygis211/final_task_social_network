import React from "react";
import style from './MyPosts.module.css'
import Post from "./Post/Post";

const MyPosts = (props: any) => {
    let postsElements =
        props.post.map((post: any) => <Post message={post.message} likes={post.likesCount} />)

    let newPostElement: any = React.createRef()

    let onAddPost = () => {
        let text = newPostElement.current.value;
        if (text) {
            props.addPost()
        }
    }

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.updateNewPostText(text)
    }

    return (
        <div className={style.content}>
            <div >
                <div>
                    <h1>Лента</h1>
                    <div>
                        <div>
                            <textarea onChange={onPostChange} 
                                      ref={newPostElement} 
                                      value={props.newPostText} />
                        </div>
                        <div>
                            <button onClick={onAddPost}>Опубликовать</button>
                        </div>
                    </div>
                    <div>
                        {postsElements}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MyPosts;