const ADD_POST = 'ADD_POST'
const UPDATE_NEW_POST_TEXT = 'UPDATE_NEW_POST_TEXT'


let initialState = {
    posts: [
        { id: 1, message: 'Hi, how are you?', likesCount: 12 },
        { id: 2, message: "It's my first post", likesCount: 15 },
        { id: 3, message: "It's my second post", likesCount: 75 },
        { id: 4, message: "Good bye", likesCount: 58 },
    ],
    newPostText: 'dima'
}

export const profileReduser = (state: any = initialState, action: any) => {
    switch (action.type) {
        case "ADD_POST":
            let newPost = {
                id: 5,
                message: state.newPostText,
                likesCount: 0
            }
            state.posts.push(newPost)
            state.newPostText = '';
            return state
        case "UPDATE_NEW_POST_TEXT":
            state.newPostText = action.newText
            return state
        default:
            return state
    }
}

export const addPostActionCreator = () => ({ type: ADD_POST })
export const updateNewPostTexttActionCreator = (text: any) =>
    ({ type: UPDATE_NEW_POST_TEXT, newText: text })