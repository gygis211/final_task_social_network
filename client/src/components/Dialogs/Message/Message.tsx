import style from "../Dialogs.module.css"

const Message = (props: any) => {
    return <div className={style.message}>{props.message}</div>
}

export default Message