"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PORT = exports.mongoUri = void 0;
exports.mongoUri = "mongodb://localhost:27017/apiDB";
exports.PORT = process.env.PORT || 2105;
