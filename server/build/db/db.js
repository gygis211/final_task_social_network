"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const config_js_1 = require("../helpers/config.js");
const db = mongoose_1.default.connect(config_js_1.mongoUri, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false
}, (err) => {
    if (!err) {
        console.log("MongoDB успешно подключился");
    }
    else {
        console.log("Ошибка в подключении к BD: " + err);
    }
});
exports.default = db;
