import express from 'express';
import { registration, authorization } from '../controllers/authController';

const authRouter = express.Router();

authRouter.post("/register", registration);
authRouter.get("/login", authorization);

export { authRouter }