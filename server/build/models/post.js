"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Post = exports.PostSchema = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const PostSchema = new Schema({
    userId: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        default: ""
    },
    attachment: {
        type: String,
        default: ""
    },
    likes: {
        type: Array,
        default: [],
    },
});
exports.PostSchema = PostSchema;
const Post = mongoose_1.default.model("Post", PostSchema);
exports.Post = Post;
PostSchema.plugin(require('mongoose-autopopulate'));
