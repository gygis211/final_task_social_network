import { updateNewPostTexttActionCreator, addPostActionCreator } from "../../../redux/profileReduser";
import MyPosts from "./MyPosts";

const MyPostsContainer = (props: any) => {

    let state = props.store.getState()

    let addPost = () => {
        props.store.dispatch(addPostActionCreator())
    }

    let postChange = (text: any) => {
        let action = updateNewPostTexttActionCreator(text)
        props.store.dispatch(action)
    }

    return (<MyPosts updateNewPostText={postChange} addPost={addPost} 
        post={state.profilePage.posts}
        newPostText={state.profilePage.newPostText} />);
}

export default MyPostsContainer;