import * as jwt from "jsonwebtoken";
import { validationResult } from "express-validator";
import { Request, Response, NextFunction } from "express";
import { User, IUser } from "../models/user";

const accessTokenSecret = "youraccesstokensecret";

interface CustomRequest<T> extends Request {
  body: T;
}

const registration = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const errors: any = validationResult(request)
        if (!errors.isEmpty()) {
            let err = errors;
            return response.status(400).send(`Ошибка: ${err.errors[0].msg}`)
        }
        let user = await User.findOne({ email: request.body.email });
        if (user) {
            return response.status(400).send(`Эта электронная почта уже используется`)
        }
        //create new user
        const newUser = await new User({ 
            username: request.body.username, 
            email: request.body.email, 
            password: request.body.password
        });
        //save new user and response
        await newUser.save();
        response.status(200).send(`Регистрация прошла успешно`);
    } catch (err) {
        response.status(500).send(`Что-то пошло не так`)
    }
}

const authorization = async function (request: CustomRequest<IUser>, response: Response) {
    const user = await User.findOne({ email: request.body.email, password: request.body.password });
    if (!user) {
        return response.status(400).send('Wrong password or email');
    }
    const accessToken = jwt.sign({ username: user.username, _id: user._id }, accessTokenSecret, { expiresIn: '5000h' });
    response.json({
        accessToken
    });

}

export { registration, authorization };
