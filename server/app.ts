import express from 'express'
import mongoose from 'mongoose'
import * as bodyParser from 'body-parser';

import { authRouter, userRouter, postRouter } from './routes/index'

const app = express()
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));
const PORT = 8080

const url = 'mongodb://localhost:27017/'
mongoose.connect(url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}, () => {
    console.log('База данных подключилась')
})

app.use('/login', authRouter)
app.use('/profile', userRouter)
app.use('/post', postRouter)

app.listen(PORT, () => {
    console.log(`Сервер запущен на порту ${PORT}`)
})