import express from 'express';

import { 
    updateUser, 
    deleteUser, 
    getUserById, 
    getAllUsers, 
    followUser, 
    unfollowUser  
} from '../controllers/userController';


const userRouter = express.Router();

userRouter.put("/:id", updateUser);
userRouter.delete("/:id", deleteUser);
userRouter.get("/:id", getUserById);
userRouter.get("/users/all", getAllUsers);
userRouter.put("/:id/follow", followUser);
userRouter.put("/:id/unfollow", unfollowUser);

export { userRouter }