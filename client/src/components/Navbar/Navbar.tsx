import React from "react";
import { NavLink } from "react-router-dom";

import style from './Navbar.module.css'

const Navbar = () => {
  return (
    <div className={style.sidebar}>
      <div className={style.item}>
        <NavLink to="/profile"><h2>Profile</h2></NavLink>
      </div>
      <div className={style.item}>
        <NavLink to="/dialogs"><h2>Messages</h2></NavLink>
      </div>
      <div className={style.item}>
        <NavLink to="/friends"><h2>Friends</h2></NavLink>
      </div>
      <div className={style.item}>
        <NavLink to="/feed"><h2>Feed</h2></NavLink>
      </div>
    </div>
  );
}

export default Navbar;