import React from "react";
import style from "./Dialogs.module.css"
import DialogItem from "./DialpgItem/DialogsItem"
import Message from "./Message/Message"
import { sendMessageCreator, updateNewMessageBodyCreator } from "../../redux/dialogsReduser";

const Dialogs = (props: any) => {

    let state = props.store.getState().messagesPage

    let dialogsElements = state.dialogs.map((d: any) => <DialogItem name={d.name} id={d.id} />);
    let messagesElements = state.messages.map((m: any) => <Message message={m.message} />);
    let newMessageText = state.newMessageText


    let onSendMessageClick = () => {
        props.store.dispatch(sendMessageCreator())
    }

    let onNewMessageChange = (e: any) => {
        let body = e.target.value
        props.store.dispatch(updateNewMessageBodyCreator(body))
    }


    return (
        <div className={style.dialogs}>
            <div className={style.dialogsItems}>
                {dialogsElements}
            </div>
            <div className={style.messages}>
                <div>{messagesElements}</div>

                <textarea value={newMessageText}
                    onChange={onNewMessageChange}
                    placeholder={'Введите сообщение...'} ></textarea>

                <button onClick={onSendMessageClick}>Отправить</button>
            </div>
        </div>
    )
}

export default Dialogs