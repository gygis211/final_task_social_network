import * as mongoose from "mongoose";
const Schema = mongoose.Schema;

interface IUser extends mongoose.Document {
  _doc: any;
  _id: any;
  userId: any;
  username: string;
  email: string;
  password: string;
  role: string;
  avatar: string;
  followers: Array<IUser>;
  followings: Array<IUser>;
}

const userSchema = new Schema(
  {
    username: {
      type: String,
      require: true,
      min: 3,
      max: 20,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      max: 50,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      min: 6,
    },
    role: {
      type: String,
      default: "user",
    },
    avatar: {
      type: String,
      default: "",
    },
    followers: {
      type: Array,
      default: [],
    },
    followings: {
      type: Array,
      default: [],
    },
  },
  { timestamps: true }
);

const User = mongoose.model<IUser>("user", userSchema);

export { userSchema, User, IUser };
