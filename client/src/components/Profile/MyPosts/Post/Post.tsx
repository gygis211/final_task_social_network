import React from "react";
import style from './Post.module.css'

const Post = (props: any) => {

    return (
        <div className={style.item}>
            <div className={style.avatar}>
                <img src="https://i.pinimg.com/736x/89/90/48/899048ab0cc455154006fdb9676964b3.jpg" alt="avatar" />
            </div>
            <div>
                <div className={style.message}>{props.message}</div>
                <span> ❤ {props.likes}</span>
            </div>
        </div>
    );
}

export default Post;