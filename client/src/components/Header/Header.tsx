import React /*, { Component }*/ from "react";
import style from './Header.module.css'
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <>
      <div className={style.header}>
        <img src="https://icon-library.com/images/logo-icon/logo-icon-28.jpg" alt="alt" />
      </div>
      <div className={style.btn}>
        <NavLink to="/login"><button>login</button></NavLink>
        <NavLink to="/registration"><button>Register</button></NavLink>
      </div>
    </>
  );
}

export default Header;
