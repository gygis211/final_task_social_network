"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllPosts = exports.getPosts = exports.likeDislikePost = exports.deletePost = exports.updatePost = exports.createPost = void 0;
const post_1 = require("../models/post");
const user_1 = require("../models/user");
//==================================================  create a post
const createPost = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const newPost = new post_1.Post(req.body);
    try {
        const savedPost = yield newPost.save();
        res.status(200).json(savedPost);
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.createPost = createPost;
//==================================================  update a post
const updatePost = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield post_1.Post.findById(req.params.id);
        if (post) {
            if (post.userId === req.body.userId) {
                yield post.updateOne({ $set: req.body });
                res.status(200).json("Окей, меняй свой пост");
            }
            else {
                res.status(403).json("Сам поменяет, если нужно будет, а ты не лезь");
            }
        }
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.updatePost = updatePost;
//==================================================  delete a post
const deletePost = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield post_1.Post.findById(req.params.id);
        if (post) {
            if (post.userId === req.body.userId) {
                yield post.deleteOne();
                res.status(200).json("Сейчас я это все почистию, уберу!");
            }
            else {
                res.status(403).json("Не нравится, не смотри, удалить чужое не можешь");
            }
        }
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.deletePost = deletePost;
//==================================================  like / dislike a post
const likeDislikePost = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield post_1.Post.findById(req.params.id);
        if (post) {
            if (!post.likes.includes(req.body.userId)) {
                yield post.updateOne({ $push: { likes: req.body.userId } });
                res.status(200).json("S... s... Senpai!!!");
            }
            else {
                yield post.updateOne({ $pull: { likes: req.body.userId } });
                res.status(200).json("Baka! :C ");
            }
        }
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.likeDislikePost = likeDislikePost;
//==================================================  get a post
const getPosts = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield post_1.Post.findById(req.params.id);
        res.status(200).json(post);
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.getPosts = getPosts;
//==================================================  get timeline posts
const getAllPosts = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const currentUser = yield user_1.User.findById(req.body.userId);
        if (currentUser) {
            const userPosts = yield post_1.Post.find({ userId: currentUser._id });
            const friendPosts = yield Promise.all(currentUser.followings.map((friendId) => {
                return post_1.Post.find({ userId: friendId });
            }));
            res.json(userPosts.concat(...friendPosts));
        }
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.getAllPosts = getAllPosts;
