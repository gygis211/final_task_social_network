
const UPDATE_NEW_MESSAGE_BODY = "UPDATE_NEW_MESSAGE_BODY"
const SEND_MESSAGE = 'SEND_MESSAGE'

let initialState = {
    dialogs: [
        { id: 1, name: 'Dima' },
        { id: 3, name: 'Pasha' },
        { id: 4, name: 'Vanya' },
        { id: 5, name: 'Petya' },
        { id: 6, name: 'Vova' },
        { id: 7, name: 'Andrey' }
    ],
    messages: [
        { id: 1, message: 'Hi' },
        { id: 2, message: 'How are you' },
        { id: 3, message: 'Yo' },
        { id: 4, message: 'Yo' },
        { id: 5, message: 'Yo' }
    ],
    newMessageText: ''
}

export const dialogsReduser = (state: any = initialState, action: any) => {
    switch (action.type) {
        case "SEND_MESSAGE":
            let body = state.newMessageText
            let id = state.messages.length
            state.messages.push({ id: id + 1, message: body })
            state.newMessageText = ''
            return state;
        case "UPDATE_NEW_MESSAGE_BODY":
            state.newMessageText = action.body
            return state;
        default:
            return state;
    }
}

export const sendMessageCreator = () => ({ type: SEND_MESSAGE })
export const updateNewMessageBodyCreator = (body: any) => 
({ type: UPDATE_NEW_MESSAGE_BODY, body: body })