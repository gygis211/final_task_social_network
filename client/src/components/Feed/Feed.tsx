import Post from "../Profile/MyPosts/Post/Post";

export default function Feed() {
    return (
        <div className="feed">
            <div className="feedWrapper">
                <Post />
            </div>
        </div>
    );
}