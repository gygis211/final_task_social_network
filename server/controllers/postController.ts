import { Request, Response } from "express";
import { Post, IPost } from "../models/post";
import { User, IUser } from "../models/user";

interface CustomRequest<T> extends Request {
  body: T;
}
//==================================================  create a post
const createPost = async (req: CustomRequest<IPost>, res: Response) => {
  const newPost = new Post(req.body);
  try {
    const savedPost = await newPost.save();
    res.status(200).json(savedPost);
  } catch (error) {
    res.status(500).json(error);
  }
};
//==================================================  update a post
const updatePost = async (req: CustomRequest<IPost>, res: Response) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post) {
      if (post.userId === req.body.userId) {
        await post.updateOne({ $set: req.body });
        res.status(200).json("Окей, меняй свой пост");
      } else {
        res.status(403).json("Сам поменяет, если нужно будет, а ты не лезь");
      }
    }
  } catch (error) {
    res.status(500).json(error);
  }
};
//==================================================  delete a post
const deletePost = async (req: CustomRequest<IPost>, res: Response) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post) {
      if (post.userId === req.body.userId) {
        await post.deleteOne();
        res.status(200).json("Сейчас я это все почистию, уберу!");
      } else {
        res.status(403).json("Не нравится, не смотри, удалить чужое не можешь");
      }
    }
  } catch (error) {
    res.status(500).json(error);
  }
};
//==================================================  like / dislike a post
const likeDislikePost = async (req: CustomRequest<IPost>, res: Response) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post) {
      if (!post.likes.includes(req.body.userId)) {
        await post.updateOne({ $push: { likes: req.body.userId } });
        res.status(200).json("S... s... Senpai!!!");
      } else {
        await post.updateOne({ $pull: { likes: req.body.userId } });
        res.status(200).json("Baka! :C ");
      }
    }
  } catch (error) {
    res.status(500).json(error);
  }
};
//==================================================  get a post
const getPosts = async (req: CustomRequest<IPost>, res: Response) => {
  try {
    const post = await Post.findById(req.params.id);
    res.status(200).json(post);
  } catch (error) {
    res.status(500).json(error);
  }
};
//==================================================  get timeline posts
const getAllPosts = async (req: CustomRequest<IPost>, res: Response) => {
  try {
    const currentUser = await User.findById(req.body.userId);
    if (currentUser) {
      const userPosts = await Post.find({ userId: currentUser._id });
      const friendPosts = await Promise.all(
        currentUser.followings.map((friendId) => {
          return Post.find({ userId: friendId });
        })
      );
      res.json(userPosts.concat(...friendPosts));
    }
  } catch (error) {
    res.status(500).json(error);
  }
};

export {
  createPost,
  updatePost,
  deletePost,
  likeDislikePost,
  getPosts,
  getAllPosts,
};
