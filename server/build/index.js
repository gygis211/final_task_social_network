"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = __importDefault(require("./db/db"));
const config_1 = require("./helpers/config");
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const indexRouter_1 = require("./routes/indexRouter");
const app = express_1.default();
//middleware
app.use(express_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(cors_1.default());
app.use("/api/auth", indexRouter_1.authRouter);
app.use("/api/users", indexRouter_1.userRouter);
app.use("/api/posts", indexRouter_1.postRouter);
const start = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield db_1.default;
        app.listen(config_1.PORT, () => [
            console.log(`Запущен сервер на порту: ${config_1.PORT}`)
        ]);
    }
    catch (e) {
        console.log(e);
    }
});
start();
