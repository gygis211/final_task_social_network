import { combineReducers, createStore } from 'redux'
import { dialogsReduser } from './dialogsReduser';
import { profileReduser } from './profileReduser';
import { sidebarReduser } from './sidebarReduser';

let redusers = combineReducers({
    profilePage: profileReduser, 
    messagesPage: dialogsReduser, 
    sidebar: sidebarReduser});

let store = createStore(redusers);

export default store
