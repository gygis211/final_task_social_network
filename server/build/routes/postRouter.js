"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postRouter = void 0;
const express_1 = __importDefault(require("express"));
const postController_1 = require("../controllers/postController");
const postRouter = express_1.default.Router();
exports.postRouter = postRouter;
postRouter.post("/", postController_1.createPost);
postRouter.put("/:id", postController_1.updatePost);
postRouter.delete("/:id", postController_1.deletePost);
postRouter.put("/:id/like", postController_1.likeDislikePost);
postRouter.get("/:id", postController_1.getPosts);
postRouter.get("/timeline/all", postController_1.getAllPosts);
