import React from 'react';
import './app.css'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import Header from './components/Header/Header';
import Navbar from './components/Navbar/Navbar';
import Profile from './components/Profile/Profile';
import Dialogs from './components/Dialogs/Dialogs';
import Feed from './components/Feed/Feed';
import Register from './components/register/Register';
import Login from './components/register/Login';

const App = (props: any) => {
  return (
    <Router>
      <div className="app-wrapper">
        <Route exact path="/registration" render={() => <Register />} />
        <Route exact path="/login" render={() => <Login />} />
        <Header />
        <Navbar />
        <Route path="/profile" render={() => <Profile store={props.store} />} />
        <Route path="/dialogs" render={() => <Dialogs store={props.store}/>} />
        <Route path="/friends" render={() => <h2>Друзья</h2>} />
        <Route path="/feed" render={() => <Feed />} />
      </div>
    </Router>
  );
}

export default App;
