import express from 'express'
import db from './db/db';
import { PORT } from "./helpers/config";
import bodyParser from 'body-parser';
import cors from 'cors'

import { authRouter, userRouter, postRouter} from './routes/indexRouter'

const app = express()


//middleware
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use("/api/auth", authRouter)
app.use("/api/users", userRouter)
app.use("/api/posts", postRouter)

const start = async() =>{
    try{
        await db
        app.listen(PORT, ()=>[
            console.log(`Запущен сервер на порту: ${PORT}`)
        ]);
    }catch (e){
        console.log(e);
    }
}

start();