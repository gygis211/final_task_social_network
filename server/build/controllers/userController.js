"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.unfollowUser = exports.followUser = exports.getAllUsers = exports.getUserById = exports.deleteUser = exports.updateUser = void 0;
const user_1 = require("../models/user");
const bcrypt_1 = __importDefault(require("bcrypt"));
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.body.userId === req.params.id) {
        if (req.body.password) {
            try {
                const salt = yield bcrypt_1.default.genSalt(5);
                req.body.password = yield bcrypt_1.default.hash(req.body.password, salt);
            }
            catch (error) {
                return res.status(500).json(error);
            }
        }
        try {
            const user = yield user_1.User.findByIdAndUpdate(req.params.id, {
                $set: req.body,
            });
            res.status(200).json("Да ты я вижу переобулся");
        }
        catch (error) {
            return res.status(500).json(error);
        }
    }
    else {
        return res.status(403).json("У тебя здесь нет власти Гендольф Серый!");
    }
});
exports.updateUser = updateUser;
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.body.userId === req.params.id) {
        try {
            yield user_1.User.findByIdAndDelete(req.params.id);
            res.status(200).json("Минус один!");
        }
        catch (error) {
            return res.status(500).json(error);
        }
    }
    else {
        return res.status(403).json("Ты что делаешь? Себе аккаунт удали!");
    }
});
exports.deleteUser = deleteUser;
const getUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_1.User.findById(req.params.id);
        if (user) {
            res.status(200).json(user);
        }
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.getUserById = getUserById;
const getAllUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const users = yield user_1.User.find({});
        if (!users) {
            return res.status(400).send(`Can't find such users`);
        }
        res.status(200).json({ users });
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.getAllUsers = getAllUsers;
const followUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.body.userId !== req.params.id) {
        try {
            const user = yield user_1.User.findById(req.params.id);
            if (user) {
                const currentUser = yield user_1.User.findById(req.body.userId);
                if (currentUser) {
                    if (!user.followers.includes(req.body.userId)) {
                        yield user.updateOne({ $push: { followers: req.body.userId } });
                        yield currentUser.updateOne({
                            $push: { followings: req.params.id },
                        });
                        res.status(200).json("Теперь вы друзья, товарищи, братья!");
                    }
                    else {
                        res.status(403).json("Так вы же и так друзья!");
                    }
                }
            }
        }
        catch (error) {
            res.status(500).json(error);
        }
    }
    else {
        res.status(403).json("Сам себе другом стать хочешь?");
    }
});
exports.followUser = followUser;
const unfollowUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.body.userId !== req.params.id) {
        try {
            const user = yield user_1.User.findById(req.params.id);
            if (user) {
                const currentUser = yield user_1.User.findById(req.body.userId);
                if (currentUser) {
                    if (user.followers.includes(req.body.userId)) {
                        yield user.updateOne({ $pull: { followers: req.body.userId } });
                        yield currentUser.updateOne({
                            $pull: { followings: req.params.id },
                        });
                        res.status(200).json("Развод и кухня пополам");
                    }
                    else {
                        res.status(403).json("Вы же и так не друзья");
                    }
                }
            }
        }
        catch (error) {
            res.status(500).json(error);
        }
    }
    else {
        res.status(403).json("Сам себе не товарищь?");
    }
});
exports.unfollowUser = unfollowUser;
