import style from './ProfileInfo.module.css'

const ProfileInfo = (props: any) => {
    return (
        <div>
            <div>
                <img src="https://i.pinimg.com/564x/85/1a/23/851a23abdcfde86d0ece7388aa307464.jpg" alt="jpeg" />
            </div>
            <div className={style.descriptionBlock}>
                ava + description
            </div>
        </div>
    );
}

export default ProfileInfo;