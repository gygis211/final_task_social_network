import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { IUser } from './user'

interface IPost extends mongoose.Document {
    userId: any,
    text: string,
    attachment: string,
    likes: Array<IUser>
}

const PostSchema = new Schema({

    userId: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        default: ""
    },
    attachment: {
        type: String,
        default: ""
    },
    likes: {
        type: Array,
        default: [],  
    },
})

const Post = mongoose.model<IPost>("Post", PostSchema)
PostSchema.plugin(require('mongoose-autopopulate'))

export { PostSchema, Post, IPost }