"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postRouter = exports.userRouter = exports.authRouter = void 0;
const authRouter_1 = require("./authRouter");
Object.defineProperty(exports, "authRouter", { enumerable: true, get: function () { return authRouter_1.authRouter; } });
const userRouter_1 = require("./userRouter");
Object.defineProperty(exports, "userRouter", { enumerable: true, get: function () { return userRouter_1.userRouter; } });
const postRouter_1 = require("./postRouter");
Object.defineProperty(exports, "postRouter", { enumerable: true, get: function () { return postRouter_1.postRouter; } });
