import { Request, Response } from "express";
import { validationResult } from "express-validator";
import { User, IUser } from "../models/user";
import bcrypt from "bcrypt";

interface CustomRequest<T> extends Request {
  body: T;
}

const updateUser = async (req: CustomRequest<IUser>, res: Response) => {
  if (req.body.userId === req.params.id) {
    if (req.body.password) {
      try {
        const salt = await bcrypt.genSalt(5);
        req.body.password = await bcrypt.hash(req.body.password, salt);
      } catch (error) {
        return res.status(500).json(error);
      }
    }
    try {
      const user = await User.findByIdAndUpdate(req.params.id, {
        $set: req.body,
      });
      res.status(200).json("Да ты я вижу переобулся");
    } catch (error) {
      return res.status(500).json(error);
    }
  } else {
    return res.status(403).json("У тебя здесь нет власти Гендольф Серый!");
  }
};

const deleteUser = async (req: CustomRequest<IUser>, res: Response) => {
  if (req.body.userId === req.params.id) {
    try {
      await User.findByIdAndDelete(req.params.id);
      res.status(200).json("Минус один!");
    } catch (error) {
      return res.status(500).json(error);
    }
  } else {
    return res.status(403).json("Ты что делаешь? Себе аккаунт удали!");
  }
};

const getUserById = async (req: CustomRequest<IUser>, res: Response) => {
  try {
    const user = await User.findById(req.params.id);
    if (user) {
      res.status(200).json(user);
    }
  } catch (error) {
    res.status(500).json(error);
  }
};

const getAllUsers = async (req: CustomRequest<IUser>, res: Response) => {
  try {
    const users = await User.find({});
    if (!users) {
      return res.status(400).send(`Can't find such users`);
    }
    res.status(200).json({users});
  } 
  catch (error) {
    res.status(500).json(error);
  }
};

const followUser = async (req: CustomRequest<IUser>, res: Response) => {
  if (req.body.userId !== req.params.id) {
    try {
      const user = await User.findById(req.params.id);
      if (user) {
        const currentUser = await User.findById(req.body.userId);
        if (currentUser) {
          if (!user.followers.includes(req.body.userId)) {
            await user.updateOne({ $push: { followers: req.body.userId } });
            await currentUser.updateOne({
              $push: { followings: req.params.id },
            });
            res.status(200).json("Теперь вы друзья, товарищи, братья!");
          } else {
            res.status(403).json("Так вы же и так друзья!");
          }
        }
      }
    } catch (error) {
      res.status(500).json(error);
    }
  } else {
    res.status(403).json("Сам себе другом стать хочешь?");
  }
};

const unfollowUser = async (req: CustomRequest<IUser>, res: Response) => {
  if (req.body.userId !== req.params.id) {
    try {
      const user = await User.findById(req.params.id);
      if (user) {
        const currentUser = await User.findById(req.body.userId);
        if (currentUser) {
          if (user.followers.includes(req.body.userId)) {
            await user.updateOne({ $pull: { followers: req.body.userId } });
            await currentUser.updateOne({
              $pull: { followings: req.params.id },
            });
            res.status(200).json("Развод и кухня пополам");
          } else {
            res.status(403).json("Вы же и так не друзья");
          }
        }
      }
    } catch (error) {
      res.status(500).json(error);
    }
  } else {
    res.status(403).json("Сам себе не товарищь?");
  }
};

export {
  updateUser,
  deleteUser,
  getUserById,
  getAllUsers,
  followUser,
  unfollowUser,
};
