"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
const express_1 = __importDefault(require("express"));
const userController_1 = require("../controllers/userController");
const userRouter = express_1.default.Router();
exports.userRouter = userRouter;
userRouter.put("/:id", userController_1.updateUser);
userRouter.delete("/:id", userController_1.deleteUser);
userRouter.get("/:id", userController_1.getUserById);
userRouter.get("/users/all", userController_1.getAllUsers);
userRouter.put("/:id/follow", userController_1.followUser);
userRouter.put("/:id/unfollow", userController_1.unfollowUser);
