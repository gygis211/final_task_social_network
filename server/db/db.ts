import mongoose from "mongoose"
import {mongoUri} from "../helpers/config.js"

const db = mongoose.connect(
    mongoUri,
    { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false
 },
    (err)=>{
        if(!err){
            console.log("MongoDB успешно подключился")
        }else{
            console.log("Ошибка в подключении к BD: " + err)
        }
    })

export default db
