import express from 'express';

import { createPost, updatePost, deletePost, likeDislikePost, getPosts, getAllPosts, } from '../controllers/postController';

const postRouter = express.Router();

postRouter.post("/", createPost);
postRouter.put("/:id", updatePost);
postRouter.delete("/:id", deletePost);
postRouter.put("/:id/like", likeDislikePost);
postRouter.get("/:id", getPosts);
postRouter.get("/timeline/all", getAllPosts);


export { postRouter }